<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i < 10; $i++) {
            User::create([
                'name' => 'User'.$i,
                'phone' => "1010101010",
                'email' => "user{$i}@evento.com",
                'pic' => UploadedFile::fake()->image("picture_{$i}.jpg", 400, 400)->store('users'),
                'password' => Hash::make('123456'),
            ]);
        }
    }
}
