<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('get-user', 'API\AuthApiController@getAuthenticatedUser');
    Route::get('all-users', 'API\UserApiController@index');
    Route::post('update-profile', 'API\UserApiController@update');
});

Route::post('register', 'API\AuthApiController@register');
Route::post('login', 'API\AuthApiController@login');
