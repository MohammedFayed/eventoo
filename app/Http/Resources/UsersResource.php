<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsersResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'users' => [
                'id' => $this->id,
                'name' => $this->name,
                'email' => $this->email,
                'created_at' => (string) $this->created_at,
                'updated_at' => (string) $this->updated_at,
                'pic' =>  $this->pic,
                'phone' => $this->phone,
            ],
        ];
    }
}
