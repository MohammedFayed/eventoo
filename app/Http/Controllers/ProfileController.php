<?php

namespace App\Http\Controllers;

use App\Http\Requests\Profile;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
class ProfileController extends Controller
{
      
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * go user view profile
     */
    public function show($id)
    {
        if (auth()->id() == $id ) {
            $user = User::findOrFail($id);
        }
        return view('user.profile',compact('user'));
    }


    /**
     * @param Profile $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Profile $request, $id)
    {

        $user = User::findOrFail($id);
        if($request->file('pic')){
          $imageName = time().'.'.request()->pic->getClientOriginalExtension();
          $file = request()->pic->move(storage_path('users'), $imageName);
        }else{
            $file = $user->pic;
        }


        $update_profile = $user->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'pic'=> $file,
            'password' => Hash::make($request->newpass),
        ]);


        if($update_profile){
            return redirect()->back()->with(['success' => 'Profile updated successfully']);
        }
        return redirect()->back()->with(['error' => 'You have error .plz try again']);

    }

}
