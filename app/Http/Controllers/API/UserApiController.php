<?php

namespace App\Http\Controllers\API;
use App\Http\Controllers\Controller;

use App\Http\Requests\Profile;
use App\Http\Resources\UsersResource;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Hash;
class UserApiController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @return user in database
     */
    public function index()
    {
        return UsersResource::collection(User::all());
    }

    /**
     * @param Profile $request
     * @return UsersResource
     * update profile user
     */
    public function update(Profile $request)
    {
        if ( JWTAuth::parseToken()->authenticate()) {
            $user_id =auth()->id();
            $users = User::find($user_id);
        }

        if($request->file('pic')){
            $imageName = time().'.'.request()->pic->getClientOriginalExtension();
            $file = request()->pic->move(public_path('users'), $imageName);
        }else{
            $imageName = $users->pic;
        }

        $users->update([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'pic'=> $imageName,
            'password' => Hash::make($request->newpass),
        ]);

        return new UsersResource($users);
    }

}
