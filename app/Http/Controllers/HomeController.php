<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\DataTables\UsersDatatable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * @return mixed
     * show users
     */
    public function index()
    {

        return view('user.users');

    }

    public function datatable(Request $request)
    {
        $users = User::get();
        return \DataTables::of($users)
            ->make(true);
    }

}
