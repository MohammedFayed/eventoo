<div id="ajax-messages"></div>





<div id="notification_success" class="alert alert-success alert-styled-left" style="display: none" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
    <strong>Success: </strong><span class="alert-msg"></span>
</div>


<?php if(session()->has('error')): ?>
    <div class="alert alert-danger alert-styled-left" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <strong>Danger: </strong><?php echo e(session()->get('error')); ?>

    </div>

<?php endif; ?>

<?php if(session()->has('success')): ?>
    <div class="alert alert-success alert-styled-left" role="alert">
        <button class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <strong>Success: </strong><?php echo e(session()->get('success')); ?>

    </div>
<?php endif; ?>