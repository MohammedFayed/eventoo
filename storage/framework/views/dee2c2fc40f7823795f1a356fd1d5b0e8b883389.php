<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row">
           
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Your Profile</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                <?php echo $__env->make('messages', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>

                                <?php echo e(Form::model($user,['route'=>['profile.update',$user->id],'method'=>'PATCH','files'=>true])); ?>


                                <div class="text-center">
                                        <img src="<?php echo e(($user->pic) ?  asset('storage/app/'.$user->pic) : 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png'); ?>" class="avatar img-circle img-thumbnail" alt="avatar" width="50%" height="50%">
                                        <h6>Upload a different photo...</h6>
                                        <input type="file" class="text-center center-block file-upload" name="pic">
                                    </div></hr><br>


                                    <div class="form-group row">
                                        <label for="username" class="col-4 col-form-label">Name*</label>
                                        <div class="col-8">
                                            <input id="name" type="text" class="form-control<?php echo e($errors->has('name') ? ' is-invalid' : ''); ?>" name="name" placeholder="userName" value="<?php echo e(($user->name) ?  $user->name  : old('name')); ?>" required autofocus>
                                        </div>
                                        <?php if($errors->has('name')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('name')); ?></strong>
                                    </span>
                                        <?php endif; ?>
                                    </div>

                                     <div class="form-group row">
                                        <label for="phone" class="col-4 col-form-label">Phone*</label>
                                        <div class="col-8">
                                            <input id="phone" type="tel" class="form-control<?php echo e($errors->has('phone') ? ' is-invalid' : ''); ?>" name="phone" placeholder="phone" value="<?php echo e(($user->phone) ?  $user->phone  : old('phone')); ?>" required autofocus>
                                        </div>
                                        <?php if($errors->has('phone')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('phone')); ?></strong>
                                    </span>
                                        <?php endif; ?>
                                    </div>

                                    <div class="form-group row">
                                        <label for="phone" class="col-4 col-form-label">Email*</label>
                                        <div class="col-8">
                                            <input id="email" type="email" class="form-control<?php echo e($errors->has('email') ? ' is-invalid' : ''); ?>" name="email" placeholder="Email" value="<?php echo e(($user->email) ?  $user->email  : old('email')); ?>" required autofocus>
                                        </div>
                                        <?php if($errors->has('email')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('email')); ?></strong>
                                    </span>
                                        <?php endif; ?>
                                    </div>

                                    
                                    <div class="form-group row">
                                        <label for="newpass" class="col-4 col-form-label">New Password</label>
                                        <div class="col-8">
                                            <input id="newpass" type="password" class="form-control<?php echo e($errors->has('newpass') ? ' is-invalid' : ''); ?>" name="newpass" placeholder="New Password" value="<?php echo e(old('newpass')); ?>"  autofocus>
                                        </div>
                                        <?php if($errors->has('newpass')): ?>
                                            <span class="invalid-feedback" role="alert">
                                        <strong><?php echo e($errors->first('newpass')); ?></strong>
                                    </span>
                                        <?php endif; ?>
                                    </div>
                                    
                                    <div class="form-group row">
                                        <div class="offset-4 col-8">
                                            <button name="submit" type="submit" class="btn btn-primary">Update My Profile</button>
                                        </div>
                                    </div>
                                <?php echo e(Form::close()); ?>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>