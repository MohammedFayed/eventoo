

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
           
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>Your Profile</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">

                                @include('messages')

                                {{Form::model($user,['route'=>['profile.update',$user->id],'method'=>'PATCH','files'=>true])}}

                                <div class="text-center">
                                        <img src="{{ ($user->pic) ?  asset('storage/app/'.$user->pic) : 'http://ssl.gstatic.com/accounts/ui/avatar_2x.png' }}" class="avatar img-circle img-thumbnail" alt="avatar" width="50%" height="50%">
                                        <h6>Upload a different photo...</h6>
                                        <input type="file" class="text-center center-block file-upload" name="pic">
                                    </div></hr><br>


                                    <div class="form-group row">
                                        <label for="username" class="col-4 col-form-label">Name*</label>
                                        <div class="col-8">
                                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" placeholder="userName" value="{{ ($user->name) ?  $user->name  : old('name') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('name'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                     <div class="form-group row">
                                        <label for="phone" class="col-4 col-form-label">Phone*</label>
                                        <div class="col-8">
                                            <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" placeholder="phone" value="{{ ($user->phone) ?  $user->phone  : old('phone') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('phone'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    <div class="form-group row">
                                        <label for="phone" class="col-4 col-form-label">Email*</label>
                                        <div class="col-8">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" placeholder="Email" value="{{ ($user->email) ?  $user->email  : old('email') }}" required autofocus>
                                        </div>
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif
                                    </div>

                                    
                                    <div class="form-group row">
                                        <label for="newpass" class="col-4 col-form-label">New Password</label>
                                        <div class="col-8">
                                            <input id="newpass" type="password" class="form-control{{ $errors->has('newpass') ? ' is-invalid' : '' }}" name="newpass" placeholder="New Password" value="{{ old('newpass') }}"  autofocus>
                                        </div>
                                        @if ($errors->has('newpass'))
                                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('newpass') }}</strong>
                                    </span>
                                        @endif
                                    </div>
                                    
                                    <div class="form-group row">
                                        <div class="offset-4 col-8">
                                            <button name="submit" type="submit" class="btn btn-primary">Update My Profile</button>
                                        </div>
                                    </div>
                                {{Form::close()}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


