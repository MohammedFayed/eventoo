@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>List Users</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">


                                <table class="table table-hover datatable">

                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>name</th>
                                        <th>email</th>
                                        <th>mobile</th>
                                    </tr>
                                    </thead>
                                </table>
                                
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection




@section('scripts')







    <script>
        $(function() {
            $('.datatable').DataTable({
                processing: true,
                searching: true,
                serverSide: true,
                ajax: '{!! url('/datatable') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'email', name: 'email' },
                    { data: 'phone', name: 'phone' },
                ],
            });
        });

    </script>
@stop

    
    
