<div id="ajax-messages"></div>


{{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}


<div id="notification_success" class="alert alert-success alert-styled-left" style="display: none" role="alert">
    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
    <strong>Success: </strong><span class="alert-msg"></span>
</div>


@if(session()->has('error'))
    <div class="alert alert-danger alert-styled-left" role="alert">
        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <strong>Danger: </strong>{{ session()->get('error')  }}
    </div>

@endif

@if(session()->has('success'))
    <div class="alert alert-success alert-styled-left" role="alert">
        <button class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
        <strong>Success: </strong>{{ session()->get('success')  }}
    </div>
@endif